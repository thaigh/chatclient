using System;
using System.Collections.Generic;
using System.Linq;
using ChatClient.Domain.Models;
using ChatClient.Domain.Repository;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace ChatClient.WebApi.Controllers {

    [EnableCors("AllowAllOrigins")]
    [Route("/api/[controller]")]
    public class ChatsController
    {
        IChatRepository _repo;

        public ChatsController(IChatRepository repo) {
            _repo = repo;

            if (_repo.GetAll().Count() == 0) {
                CreateSampleChatData();
            }
        }

        private void CreateSampleChatData() {
            var john = new Person { Id = 1, Name = "John" };
            var jane = new Person { Id = 2, Name = "Jane" };

            var convo = new Conversation {
                Id = 1,
                Sender = john,
                Receiver = jane
            };

            _repo.Add(new ChatMessage {
                Id = 1,
                Timestamp = DateTime.Now.AddSeconds(-5),
                Message = "Hello",
                Conversation = convo,
                Sender = john
            });
            _repo.Add(new ChatMessage {
                Id = 2,
                Timestamp = DateTime.Now.AddSeconds(-1),
                Message = "Hello Back!",
                Conversation = convo,
                Sender = jane
            });
        }

        // GET: api/chats/
        [HttpGet]
        public IEnumerable<ChatMessage> GetAll() {
            return _repo.GetAll();
        }

        // GET: api/chats/1
        [HttpGet("{id}", Name="GetChatMessage")]
        public IActionResult GetById(int id) {
            var item = _repo.Get(id);
            if (item == null) return new NotFoundResult();
            return new ObjectResult(item);
        }

        //POST: api/chats
        [HttpPost]
        public IActionResult Create([FromBody] ChatMessage message) {
            if (message == null) return new BadRequestResult();

            var existingMessage = _repo.Get(message.Id);
            if (existingMessage != null) return new BadRequestResult();

            _repo.Add(message);

            return new CreatedAtRouteResult(
                "GetChatMessage",
                new { id = message.Id },
                message
            );
        }

        // PUT: api/chats/1
        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] ChatMessage message) {
            if (message == null || message.Id != id)
                return new BadRequestResult();

            var originalMessage = _repo.Get(message.Id);
            if (originalMessage == null) return new NotFoundResult();

            _repo.Update(message.Id, message);
            return new NoContentResult();
        }

        // DELETE: api/chats
        [HttpDelete("{id}")]
        public IActionResult Delete(int id) {
            var chat = _repo.Get(id);
            if (chat == null) return new NotFoundResult();

            _repo.Remove(id);
            return new NoContentResult();
        }

        [HttpGet("{id}")]
        public IEnumerable<ChatMessage> GetChatsForConversation(int conversationId) {
            return _repo.GetAll();
        }
    }

}

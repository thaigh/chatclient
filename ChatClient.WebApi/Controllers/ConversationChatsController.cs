using System.Collections.Generic;
using System.Linq;
using ChatClient.Domain.Models;
using ChatClient.Domain.Repository;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace ChatClient.WebApi.Controllers {

    [EnableCors("AllowAllOrigins")]
    [Route("api/Conversations/{conversationId:int}/Messages")]
    public class ConversationChatsController {

        private IChatRepository _repo;

        public ConversationChatsController(IChatRepository repo) { this._repo = repo; }

        private IEnumerable<ChatMessage> GetChatsForConversation(int conversationId) {
            return _repo.GetAll().Where(m => m.ConversationId == conversationId);
        }

        [HttpOptions]
        public IActionResult Options() {
            return new ObjectResult(true);
        }

        [HttpGet]
        public IEnumerable<ChatMessage> GetAll(int conversationId, int limit = 0) {
            var chats = GetChatsForConversation(conversationId);

            if (limit > 0)
                return chats.OrderByDescending(m => m.Timestamp).Take(limit);

            return chats;
        }

        [HttpGet("{messageId}")]
        public IActionResult GetSingleMessage(int conversationId, int messageId) {
            var msg = _repo.GetAll()
                .FirstOrDefault(m => m.ConversationId == conversationId && m.Id == messageId);

            if (msg == null) return new NotFoundResult();
            return new ObjectResult(msg);
        }

    }

}

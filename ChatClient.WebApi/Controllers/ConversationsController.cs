using System;
using System.Collections.Generic;
using ChatClient.Domain.Models;
using ChatClient.Domain.Repository;
using Microsoft.AspNetCore.Mvc;

namespace ChatClient.WebApi.Controllers {

    [Route("/api/[controller]")]
    public class ConversationsController {

        IConversationRepository _repo;

        public ConversationsController(IConversationRepository repo) { this._repo = repo; }

        [HttpGet]
        public IEnumerable<Conversation> GetAll() {
            return _repo.GetAll();
        }

        [HttpGet("{id}", Name="GetConversation")]
        public IActionResult GetById(int id) {
            var conv = _repo.Get(id);
            if (conv == null) return new NotFoundResult();
            return new ObjectResult(conv);
        }

    }

}

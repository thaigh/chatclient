﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChatClient.Domain;
using ChatClient.Domain.Repository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace ChatClient.WebApi
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }

        private string Auth0Domain {
            get {
                string auth0Domain = Configuration["Auth0:Domain"];
                string domain = $"https://{auth0Domain}/";
                return domain;
            }
        }

        private string Auth0ApiIdentifier { get {
            return Configuration["Auth0:ApiIdentifier"];
        }}

        private string ChatDatabaseConnectionString {
            get { return Configuration["ConnectionStrings:ChatDatabase"]; }
        }

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc();

            services.AddAuthorization();

            services.AddEntityFrameworkSqlite()
                .AddDbContext<ChatContext>();

            // services.AddIdentity<ApplicationUser, IdentityRole>()
            //     .AddEntityFrameworkStores<ApplicationDbContext>()
            //     .AddDefaultTokenProviders();

            services.AddScoped<IChatRepository, ChatRepository>();
            services.AddScoped<IConversationRepository, ConversationRepository>();

            services.AddCors(options => {
                options.AddPolicy("AllowAllOrigins", builder => builder.AllowAnyOrigin().AllowAnyMethod());
            });

            }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            var jwtOptions = new JwtBearerOptions {
                Audience = Auth0ApiIdentifier,
                Authority = Auth0Domain
            };
            app.UseJwtBearerAuthentication(jwtOptions);

            app.UseCors("AllowAllOrigins");
            app.UseMvc();
        }
    }
}

import { CallbackComponent } from './components/callback/callback.component';
import { LoginComponent } from './components/login/login.component';
import {RouterModule} from '@angular/router';

export const AuthRoutes = RouterModule.forChild([
    { path: 'auth/login', component: LoginComponent },
    { path: 'auth/callback', component: CallbackComponent }
]);

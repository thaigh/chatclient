import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { LoginComponent } from './components/login/login.component';
import { AuthService } from './services/auth.service';
import { CallbackComponent } from './components/callback/callback.component';

@NgModule({
    imports: [
        CommonModule, RouterModule
    ],
    declarations: [LoginComponent, CallbackComponent],
    providers: [AuthService ],
    exports: [ LoginComponent ]
})
export class AuthModule { }

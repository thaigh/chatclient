import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import * as auth0 from 'auth0-js';

const AccessTokenKey    = 'access_token';
const IdTokenKey        = 'id_token';
const ExpiresAtKey      = 'expires_at';

@Injectable()
export class AuthService {


    redirectUrl: string = null;

    auth0 = new auth0.WebAuth({
        clientID: 'wt0ycY3VFHLR5yEs7zzBHJbH1Z0hCt3y',
        domain: 'tylerhaigh.au.auth0.com',
        responseType: 'token id_token',
        audience: 'https://tylerhaigh.au.auth0.com/userinfo',
        redirectUri: 'http://localhost:4200/auth/callback',
        scope: 'openid'
    });


    constructor(
        private router: Router
    ) { }

    login(): void {
        this.auth0.authorize(null);
    }

    loginWithRedirect(redirectUrl: string) {
        this.redirectUrl = redirectUrl;
        this.login();
    }

    public handleAuthentication(): void {
        this.auth0.parseHash((err, authResult) => {
            if (authResult && authResult.accessToken && authResult.idToken) {
                window.location.hash = '';
                this.setSession(authResult);

                const redirectTo = this.redirectUrl || '/';
                if (this.redirectUrl) { this.redirectUrl = null; }

                this.router.navigate([redirectTo]);
            } else if (err) {
                this.router.navigate(['/']);
                console.log(err);
            }
        });
    }

    private setSession(authResult): void {
        // Set the time that the access token will expire at
        const expiresAt = JSON.stringify((authResult.expiresIn * 1000) + new Date().getTime());
        localStorage.setItem(AccessTokenKey, authResult.accessToken);
        localStorage.setItem(IdTokenKey, authResult.idToken);
        localStorage.setItem(ExpiresAtKey, expiresAt);
    }

    public logout(): void {
        // Remove tokens and expiry time from localStorage
        localStorage.removeItem(AccessTokenKey);
        localStorage.removeItem(IdTokenKey);
        localStorage.removeItem(ExpiresAtKey);

        // Go back to the home route
        this.router.navigate(['/']);
    }

    public isAuthenticated(): boolean {
        // Check whether the current time is past the
        // access token's expiry time
        const expiresAt = JSON.parse(localStorage.getItem(ExpiresAtKey));
        return new Date().getTime() < expiresAt;
    }

}

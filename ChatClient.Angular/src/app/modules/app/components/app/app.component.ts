import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from './../../../auth/services/auth.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    providers: [AuthService]
})
export class AppComponent implements OnInit {
    title = 'app';

    constructor(
        private auth: AuthService,
        private router: Router
    ) {
        auth.handleAuthentication();
    }


    ngOnInit() {
        if (!this.auth.isAuthenticated()) {
            this.router.navigate(['/auth/login']);
        }
    }



}

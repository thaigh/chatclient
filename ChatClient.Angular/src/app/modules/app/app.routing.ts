import { HomeComponent } from './components/home/home.component';
import { RouterModule } from '@angular/router';

import { NotFoundComponent } from './components/not-found/not-found.component';

export const AppRoutes = RouterModule.forRoot([
    { path: '', component: HomeComponent },
    { path: '**', component: NotFoundComponent }
]);

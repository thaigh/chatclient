
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { MessagesModule } from './../messages/messages.module';
import { AuthModule } from './../auth/auth.module';
import { PepperCommonModule } from './../common/common.module';

import { AppRoutes } from './app.routing';
import { AuthRoutes } from './../auth/auth.routing';
import { MessageRoutes } from './../messages/messages.routing';

import { AppComponent } from './components/app/app.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { HomeComponent } from './components/home/home.component';

@NgModule({
    declarations: [
        AppComponent,
        NotFoundComponent,
        HomeComponent
    ],
    imports: [
        BrowserModule, MessagesModule, AuthModule, RouterModule, PepperCommonModule,
        AuthRoutes, MessageRoutes, AppRoutes
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }

import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { PepperCommonModule } from './../common/common.module';

import { MessageService } from './services/message/message.service';
import { ConversationService } from './services/conversation/conversation.service';

import { ConversationListComponent } from './components/conversation-list/conversation-list.component';
import { ConversationMessagesComponent } from './components/conversation-messages/conversation-messages.component';
import { MessageComponent } from './components/message/message.component';

@NgModule({
    imports: [
        CommonModule, FormsModule, ReactiveFormsModule, RouterModule, PepperCommonModule, HttpClientModule
    ],
    declarations: [ MessageComponent, ConversationListComponent, ConversationMessagesComponent],
    providers: [MessageService, ConversationService],
    exports: [MessageComponent, ConversationListComponent, ConversationMessagesComponent]
})
export class MessagesModule { }

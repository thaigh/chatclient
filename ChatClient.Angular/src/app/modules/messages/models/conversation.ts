import { Message } from './message';
import { Person } from './person';

export interface IConversation {
    id: number;

    senderId: number;
    sender?: Person;

    receiverId: number;
    receiver?: Person;

    messages?: Message[];
}

export class Conversation implements IConversation {
    id: number;

    senderId: number;
    sender?: Person = new Person();

    receiverId: number;
    receiver?: Person = new Person();

    messages?: Message[] = [];

    constructor(c: IConversation = { id: 0, senderId: 1, receiverId: 2 }) {
        this.id = c.id;
        this.senderId = c.senderId;
        this.receiverId = c.receiverId;
    }

    public otherUser(personId: number): Person {
        return Conversation.otherUser(this, personId);
    }

    static otherUser(c: Conversation, personId: number) {
        if (c.senderId !== personId && c.receiverId !== personId) {
            throw new Error(`No User with ID ${personId} is a participant in c conversation`)
        }

        return c.senderId === personId
            ? c.receiver
            : c.sender;
    }
}

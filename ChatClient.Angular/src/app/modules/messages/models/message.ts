import { Person } from './person';
export interface IMessage {
    id?: number;
    message: string;
    timestamp: Date;

    conversationId: number;

    senderId: number;
    sender: Person;
}

export class Message implements IMessage {
    id?: number;
    message: string;
    timestamp: Date;

    conversationId: number;

    senderId: number;
    sender: Person;

    constructor(m: IMessage = { id: 0, message: '', senderId: 0, timestamp: new Date(), sender: new Person(), conversationId: 0 }) {
        this.id = m.id
        this.message = m.message;
        this.timestamp = m.timestamp;

        this.conversationId = m.conversationId;

        this.senderId = m.senderId;
        this.sender = m.sender;
    }
}

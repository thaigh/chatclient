export interface IPerson {
    id: number;
    name: string;
}

export class Person implements IPerson {
    id: number;
    name: string;

    constructor(p: IPerson = { id: 0, name: '' }) {
        this.id = p.id;
        this.name = p.name;
    }
}

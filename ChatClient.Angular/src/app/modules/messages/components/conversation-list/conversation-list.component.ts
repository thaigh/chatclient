import { Person } from './../../models/person';
import { Conversation } from './../../models/conversation';
import { ConversationService } from './../../services/conversation/conversation.service';
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-conversation-list',
    templateUrl: './conversation-list.component.html',
    styleUrls: ['./conversation-list.component.css']
})
export class ConversationListComponent implements OnInit {

    conversations: Conversation[] = [];
    thisUser = new Person({ id: 1, name: 'John' });
    isLoading = false;

    constructor(
        private conversationService: ConversationService
    ) { }

    ngOnInit() {
        this.isLoading = true;
        this.conversationService
            .getConversations(this.thisUser.id)
            .catch( (err) => { console.error(err); this.isLoading = false; return []; })
            .subscribe((convs) => {
                this.conversations = convs;
                this.isLoading = false;
            })

    }

    otherUserForConversation(c: Conversation): Person {
        //var other = c.otherUser(this.thisUser.id);
        var other = Conversation.otherUser(c, this.thisUser.id);
        return other;
    }

}

import { Observable } from 'rxjs/Rx';
import { ConversationService } from './../../services/conversation/conversation.service';
import { ActivatedRoute } from '@angular/router';
import { Conversation } from './../../models/conversation';
import { Person } from './../../models/person';

import { MessageService } from './../../services/message/message.service';
import { Message } from './../../models/message';
import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

@Component({
    selector: 'app-conversation-messages',
    templateUrl: './conversation-messages.component.html',
    styleUrls: ['./conversation-messages.component.css']
})
export class ConversationMessagesComponent implements OnInit {

    conversationId: number;
    conversation: Conversation;
    sender = new Person({ id: 1, name: 'John' });
    otherPerson: Person = new Person();
    isLoading = false;

    messages: Message[];

    form: FormGroup;

    constructor(
        private conversationService: ConversationService,
        private messageService: MessageService,
        private fb: FormBuilder,
        private route: ActivatedRoute
    ) { }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.conversationId = params['id'];

            this.isLoading = true;
            var convos = this.conversationService.getConversation(this.conversationId);
            var msgs = this.messageService.getMessages(this.conversationId);

            Observable.forkJoin(convos, msgs)
                .map( (joined) => { return { conversation: joined[0], messages: joined[1] }})
                .catch( (err) => {
                    console.error(err);
                    this.isLoading = false;
                    return Observable.of({conversation: null, messages: []});
                })
                .subscribe( (data)=> {
                    this.conversation = data.conversation;
                    this.messages = data.messages;

                    this.otherPerson = Conversation.otherUser(this.conversation, this.sender.id);
                    this.isLoading = false;
                })

        });


        this.form = this.fb.group({
            message: ['', [Validators.required], null]
        });
    }

    sendMessage() {
        const model = this.form.value;

        // Don't allow only \n characters
        const regex = new RegExp('[^\n]')
        const containsSomething = regex.test(model.message);
        if (!containsSomething) { return; }

        const msg = new Message({
            conversationId: this.conversation.id,
            message: model.message,
            senderId: this.sender.id,
            timestamp: new Date(),
            sender: this.sender
        });

        this.messages.push(msg);
        this.form.reset();

    }

}

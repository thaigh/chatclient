import { Message } from './../../models/message';
import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-message',
    templateUrl: './message.component.html',
    styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {

    @Input('message') message: Message = new Message();
    @Input('sentByThisUser') sentByThisUser: boolean = false;

    private showTimestamp: boolean = false;

    constructor() { }

    ngOnInit() {
    }

    toggleShowTimestamp() {
        this.showTimestamp = !this.showTimestamp;
    }

}

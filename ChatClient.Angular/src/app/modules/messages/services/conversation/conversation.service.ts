import { HttpClient } from '@angular/common/http';
import { Person } from './../../models/person';
import { Conversation } from './../../models/conversation';
import { Injectable } from '@angular/core';

import {Observable} from 'rxjs/Rx';

import 'rxjs/add/operator/map'
import 'rxjs/add/operator/toPromise'

const john = new Person({ id: 1, name: 'John' });
const jane = new Person({ id: 2, name: 'Jane' });
const c1 = new Conversation({ id: 1, senderId: 1, sender: john, receiverId: 2, receiver: jane, messages: [] });

const ConversationApiUrl = 'http://localhost:5000/api/Conversations'

@Injectable()
export class ConversationService {

    constructor(
        private http: HttpClient
    ) { }

    public getConversations(personId: number): Observable<Conversation[]> {
        return this.http
            .get<Conversation[]>(ConversationApiUrl);

        // return new Promise<Conversation[]>(
        //     (resolve, reject) => {
        //         setTimeout(() => resolve([ c1 ]), 2000);
        //     }
        // );
    }

    getConversation(id: number): Observable<Conversation> {
        // return new Promise<Conversation> (
        //     (resolve, reject) => {
        //         setTimeout(() => resolve(c1), 2000);
        //     }
        // );

        return this.http
            .get<Conversation>(ConversationApiUrl + `/${id}`);
    }

}

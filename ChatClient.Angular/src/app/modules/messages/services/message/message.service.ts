import { Observable } from 'rxjs/Rx';
import { HttpClient } from '@angular/common/http';
import { Person } from './../../models/person';
import { Message } from './../../models/message';
import { Injectable } from '@angular/core';


const m1 = new Message({
    message: 'Hello',
    senderId: 1,
    conversationId: 1,
    sender: new Person({id: 1, name: 'John'}),
    timestamp: new Date(2017, 1, 1, 6, 28, 12)
});

const m2 = new Message({
    message: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been...',
    senderId: 2,
    conversationId: 1,
    sender: new Person({id: 2, name: 'Jane'}),
    timestamp: new Date(2017, 1, 1, 6, 30, 12)
});


@Injectable()
export class MessageService {

    constructor(
        private http: HttpClient
    ) { }

    getMessages(conversationId: number): Observable<Message[]> {

        return this.http.get<Message[]>(`http://localhost:5000/api/Conversations/1/Messages`)

        // const promise = new Promise<Message[]>(
        //     (resolve, reject) => {
        //         setTimeout(() => resolve([ m1, m2 ]), 2000);
        //     }
        // );

        // return promise;
    }

    postMessage(message: Message) {
        console.log('Sending message to server', message);
    }

}

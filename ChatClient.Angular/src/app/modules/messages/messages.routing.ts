import { ConversationListComponent } from './components/conversation-list/conversation-list.component';
import { ConversationMessagesComponent } from './components/conversation-messages/conversation-messages.component';
import {RouterModule} from '@angular/router';

export const MessageRoutes = RouterModule.forChild([
    { path: 'conversations', component: ConversationListComponent },
    { path: 'conversations/:id', component: ConversationMessagesComponent }
]);

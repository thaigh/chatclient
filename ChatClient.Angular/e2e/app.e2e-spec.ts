import { ChatClient.AngularPage } from './app.po';

describe('chat-client.angular App', () => {
    let page: ChatClient.AngularPage;

    beforeEach(() => {
        page = new ChatClient.AngularPage();
    });

    it('should display welcome message', () => {
        page.navigateTo();
        expect(page.getParagraphText()).toEqual('Welcome to app!!');
    });
});

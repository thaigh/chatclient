﻿using System;
using ChatClient.Domain;
using ChatClient.Domain.Models;

namespace ChatClient.ConsoleTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            using (ChatContext context = new ChatContext()) {
                context.ChatMessages.Add(new ChatMessage {
                    Message = "Hello World",
                    PersonFrom = new Person { Id = 1, Name = "John" },
                    PersonTo   = new Person { Id = 2, Name = "Jane" }
                });

                context.SaveChanges();
            }
        }
    }
}

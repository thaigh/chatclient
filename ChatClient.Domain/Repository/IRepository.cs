using System;
using System.Collections.Generic;

namespace ChatClient.Domain.Repository {

    public interface IRepository<TData, TKey> : IDisposable {

        IEnumerable<TData> GetAll();
        TData Get(TKey id);
        void Update(TKey id, TData instance);
        void Remove(TKey id);
        void Add(TData item);
    }

}
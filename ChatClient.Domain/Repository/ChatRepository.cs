using System;
using System.Collections.Generic;
using System.Linq;
using ChatClient.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace ChatClient.Domain.Repository {

    public interface IChatRepository : IRepository<ChatMessage, int> { }

    public class ChatRepository : IChatRepository
    {
        ChatContext db = new ChatContext();

        public void Add(ChatMessage item)
        {
            db.ChatMessages.Add(item);
            db.SaveChanges();
        }

        public ChatMessage Get(int id)
        {
            return GetAll().FirstOrDefault(c => c.Id == id);
        }

        public IEnumerable<ChatMessage> GetAll()
        {
            return db.ChatMessages
                .Include(c => c.Sender);
        }

        public void Remove(int id)
        {
            var chat = ResolveChatFromId(id);

            db.ChatMessages.Remove(chat);
            db.SaveChanges();
        }

        private ChatMessage ResolveChatFromId(int id) {
            var chat = Get(id);
            if (chat == null) throw new Exception("Chat not found"); // TODO: Use NotFoundException
            return chat;
        }

        public void Update(int id, ChatMessage updatedMessage)
        {
            var chat = ResolveChatFromId(id);

            chat.Message = updatedMessage.Message;
            chat.Timestamp = updatedMessage.Timestamp;

            // db.ChatMessages.Attach(updatedMessage);
            // var entry = db.Entry(updatedMessage);

            // entry.Property(e => e.Message).IsModified = true;
            // entry.Property(e => e.TimeSent).IsModified = true;

            db.SaveChanges();
        }

        #region IDisposable Support
        private bool hasBeenDisposed = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!hasBeenDisposed && disposing)
            {
                db.Dispose();
                db = null;
                hasBeenDisposed = true;
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

    }

}

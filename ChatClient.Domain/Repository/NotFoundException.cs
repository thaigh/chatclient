using System;

namespace ChatClient.Domain.Repository {

    public class NotFoundException : Exception {

        public NotFoundException() : base("Item not found in repository") { }

        public NotFoundException(string message) : base(message) { }

    }

}

using System;
using System.Collections.Generic;
using System.Linq;
using ChatClient.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace ChatClient.Domain.Repository {

    public interface IConversationRepository : IRepository<Conversation, int> { }

    public class ConversationRepository : IConversationRepository
    {

        private ChatContext db = new ChatContext();

        public void Add(Conversation item)
        {
            db.Conversations.Add(item);
            db.SaveChanges();
        }

        public Conversation Get(int id)
        {
            return GetAll().FirstOrDefault( c => c.Id == id);
        }

        public IEnumerable<Conversation> GetAll()
        {
            return db.Conversations
                .Include(c => c.Sender)
                .Include(c => c.Receiver);
        }

        public void Remove(int id)
        {
            var conv = ResolveFromId(id);
            db.Remove(conv);
            db.SaveChanges();
        }

        private Conversation ResolveFromId(int id) {
            var conv = Get(id);
            if (conv == null) throw new NotFoundException();
            return conv;
        }

        public void Update(int id, Conversation instance)
        {
            // Do nothing...
        }

        #region IDisposable Support
        private bool hasBeenDisposed = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!hasBeenDisposed && disposing)
            {
                db.Dispose();
                db = null;
                hasBeenDisposed = true;
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

    }

}

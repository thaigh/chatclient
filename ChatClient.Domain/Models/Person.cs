using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ChatClient.Domain.Models {
    
    [Table("Persons")]
    public class Person {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
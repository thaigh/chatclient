using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ChatClient.Domain.Models {

    [Table("ChatMessages")]
    public class ChatMessage {
        public int Id { get; set; }
        public string Message { get; set; }
        public DateTime Timestamp { get; set; }

        public int ConversationId { get; set; }
        public virtual Conversation Conversation { get; set; }

        public int SenderId { get; set; }
        public virtual Person Sender { get; set; }
    }

}

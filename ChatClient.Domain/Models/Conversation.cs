using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ChatClient.Domain.Models
{
    [Table("Conversations")]
    public class Conversation {
        
        public int Id { get; set; }
        
        public int SenderId { get; set; }
        public virtual Person Sender { get; set; }

        public int ReceiverId { get; set; }
        public virtual Person Receiver { get; set; }

        public virtual List<ChatMessage> Messages { get; set; }
    }
}
﻿using System;
using ChatClient.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace ChatClient.Domain
{
    public class ChatClientContext {
        public const string ConnectionString = "Data Source=Chats.db";
    }

    public class ChatContext : DbContext
    {
        public DbSet<ChatMessage> ChatMessages { get; set; }
        public DbSet<Conversation> Conversations { get; set; }
        public DbSet<Person> Persons { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder builder) {
            builder.UseSqlite(ChatClientContext.ConnectionString);
        }
    }
    
}
